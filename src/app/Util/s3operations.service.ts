import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { environment } from '../environment';

@Injectable()
export class S3OperationsService {

  private s3UploadTuneUrl = environment.apiEndPoint + 'S3/upload/song';
  private s3UploadImageUrl = environment.apiEndPoint + 'S3/upload/image';

  constructor(private http: Http) { }

  uploadTune(file: File): Promise<string>  {
    return this.post(file, this.s3UploadTuneUrl);
  }

  uploadImage(file: File): Promise<string>  {
    return this.post(file, this.s3UploadImageUrl);
  }

  private post(file: File, url:string): Promise<string> {
    return new Promise((resolve, reject) => {
        let formData: FormData = new FormData(),
            xhr: XMLHttpRequest = new XMLHttpRequest();
        formData.append('file',file);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.response);
                }
            }
        };
        xhr.open('POST', url, true);
        xhr.send(formData);
    });
  }

  private handleError(error: any) {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}