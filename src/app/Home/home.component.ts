import { Component }  from '@angular/core';
import {Subscription} from "rxjs";
import {Observable} from 'rxjs/Rx';

import { Song }        from '../Song/song';
import { SongService } from '../Song/song.service';

@Component({
  selector: 'my-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
})
export class HomeComponent {
  title = '';
  song: Song;
  error: any;
  isPlaying = false;
  timeCount: number;
  subscription: Subscription;

  constructor(private songService: SongService) {}

   ngOnInit() {

    if(!this.song) {
      this.song = new Song;
      this.song.duration = 0;
    }
    this.timeCount = 0;
   }

  onPlayClick() {
    this.isPlaying = !this.isPlaying;
    if (this.isPlaying == true) {
      let timer = Observable.timer(1000,1000);
      this.subscription = timer.subscribe(t=>{
        this.timeCount += 1;
        if (this.timeCount >= this.song.duration - 10) {
          this.songService
            .getTopVote()
            .then(song => {
              if(song == null) {
                this.subscription.unsubscribe();
                this.isPlaying = false;
                return;
              }
              if (this.song.duration == 0) {
                this.timeCount = 0;
              } else {
                this.timeCount = -10;
              }
              this.song = song;
              console.log("Succesfully retreived top vote song!");
              
            })
            .catch(error => this.error = error); // TODO: Display error message 
        }
      });
    } else {
      this.subscription.unsubscribe();
    }
  }
}
