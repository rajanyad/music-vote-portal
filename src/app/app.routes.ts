import { Routes, RouterModule }  from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { HomeComponent } from './Home/home.component';
import { SongLandingComponent } from './Song/song-landing.component';

const appRoutes: Routes = [  
    {
      path: '', 
      redirectTo: 'song', 
      pathMatch: 'full'
    },
    {
      path: 'home',
      component: HomeComponent
    },
    {
      path: 'song',
      component: SongLandingComponent
    }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);