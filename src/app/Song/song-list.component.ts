import { Component, Input, Output, OnInit, EventEmitter, ViewChild } from '@angular/core';

import { Song }        from './song';
import { SongService } from './song.service';

import { NguiMessagePopupComponent, NguiPopupComponent } from '@ngui/popup';

@Component({
  selector: 'my-song-list',
  templateUrl: 'song-list.component.html',
  styleUrls: ['song-list.component.scss']
})
export class SongListComponent implements OnInit {

  songs: Song[];
  isLoading: boolean;
  error: any;
  @ViewChild(NguiPopupComponent) popup: NguiPopupComponent;

  constructor(
    private songService: SongService) {
  }
  ngOnInit() {
   
    this.isLoading = true;

    this.songService
        .get()
        .then(songs => {
          this.isLoading = false;
          this.songs = songs;
          console.log("Succesfully retreived songs!");
        })
        .catch(error => this.error = error); // TODO: Display error message 
  }

  edit(song: Song) {
    song.isEditClicked = !song.isEditClicked;
  }

  delete(size, title, song: Song) {
    
    this.popup.open(NguiMessagePopupComponent, {
      classNames: size,
      title: title,
      message: "Are you sure you want to delete this song?",
      buttons: {
        OK: () => {
          this.songService.delete(song.id);
          var index = this.songs.indexOf(song, 0);
          if (index > -1) {
            this.songs.splice(index, 1);
          }
          this.popup.close();
        },
        CANCEL: () => {
          this.popup.close();
        }
      }
    });
  }

  onChangePlayed(song: Song) {
    console.log("On change played!");
    song.hasPlayed = !song.hasPlayed;
    this.songService.updateHasPlayed(song);
  }
}
