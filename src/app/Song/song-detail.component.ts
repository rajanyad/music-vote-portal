import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';

import { Song }        from './song';
import { SongService } from './song.service';

import { S3OperationsService }  from '../Util/s3operations.service';
import { NguiMessagePopupComponent, NguiPopupComponent } from '@ngui/popup';

@Component({
  selector: 'my-song-detail',
  templateUrl: 'song-detail.component.html',
  styleUrls: ['song-detail.component.scss']
})
export class SongDetailComponent implements OnInit {
  
  @Input() song: Song;
  @Output() saveSong = new EventEmitter;
  error: any;
  @ViewChild(NguiPopupComponent) popup: NguiPopupComponent;

  isSaved: boolean;
  isSpinnerOn: boolean;

  isLanguageChosen: boolean;
  isTopicClicked: boolean;
  isFileUploading: boolean;
  isFileBig: boolean;

  languages: string[];
  genres: string[];
  votes: number[];

  constructor(
    private songService: SongService,
    private s3Service: S3OperationsService) {
  }

  ngOnInit() {

    if(!this.song) {
      this.song = new Song;
      this.song.hasPlayed = false;
      this.song.duration = 0;
    }

    if(this.song.releaseDate) {
      this.song.formatedDate = this.formatReleaseDate(this.song.releaseDate);  
    }

    this.languages = ['Hindi', 'English', 'Bengali', 'Marathi', 'Telegu', 'Kannada']
    this.genres = ['Romantic','Pop','Hard Rock']
    this.votes = [];
    for( var i = 0 ; i < 100; i++ ) {
      this.votes.push(i);
    }
    this.isSaved = false;
    this.isFileUploading = false;
    this.isFileBig = false;
    this.isSpinnerOn = false;
  }

  save() {

    this.isSpinnerOn = true;

    // Release Date check
    if(this.song.formatedDate){
      this.song.releaseDate = new Date(this.song.formatedDate);
    } 

    this.songService
        .save(this.song)
        .then(song => {
            this.isSaved = true;
            this.isSpinnerOn = false;
            console.log("Saved!");
            this.saveSong.emit(song);
          }
        )
        .catch(error => {
            this.error = error;
            
            if(this.error.status == 302) {
              this.popupMsg('small', 'Duplicate record','Please go to List to update!');
            } else {
              console.log(this.error);
            }
          }); 
  }

  onTrackUpload($event: any) {
    var songFile = $event.target.files[0];
    
    var video = document.createElement('video');
    var duration = 0;
    
    video.preload = 'metadata';
    video.onloadedmetadata = function() {
      window.URL.revokeObjectURL(video.src);
      duration = video.duration;
    }
    video.src = URL.createObjectURL(songFile);   

    this.isFileBig = false;
    if(songFile.size > 1000000) {
      this.isFileBig = true;
      return;
    }
    this.isFileUploading = true;
    this.s3Service
        .uploadTune(songFile)
        .then(songUrl => {
          this.song.url = songUrl;
          this.isFileUploading = false;
          this.song.duration = duration;
          console.log(this.song.duration); 
          console.log(this.song.url);
        })
        .catch(error => this.error = error); // TODO: Display error message
  }

  private formatReleaseDate(releaseDate: Date): string {
    var num = Number(releaseDate);
    var date = new Date(num);
    var year = date.getFullYear();
    var month = date.getMonth();
    var monthStr;

    if(month < 10) {
      monthStr = "0" + month;
    } else {
      monthStr = month;
    }
    var day = date.getDay();
    var dayStr;
    if(day == 0) {
      day = 1;
    }
    if(day < 10) {
      dayStr = "0" + day;
    } else {
      dayStr = day;
    }
    var formatDate = year + "-" + monthStr + "-" + dayStr;
    
    return formatDate;
  }

  private popupMsg(size, title, message) {

    this.popup.open(NguiMessagePopupComponent, {
      classNames: size,
      title: title,
      message: message,
      buttons: {
        OK: () => {
          this.popup.close();
        }
      }
    });
  }
}
