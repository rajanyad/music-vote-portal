import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Song } from './song';

import { environment } from '../environment';

@Injectable()
export class SongService {

  private songSaveUrl = environment.apiEndPoint + 'song/save'; 
  private songGetUrl = environment.apiEndPoint + 'song/get/all';
  private songDeleteUrl = environment.apiEndPoint + 'song/delete/'; 
  private songGetTopVoteUrl = environment.apiEndPoint + 'song/get/topvote';
  private songUpdateHasPlayedUrl = environment.apiEndPoint + '/song/update/';

  constructor(private http: Http) { }


  save(song: Song): Promise<Song>  {
    return this.post(song);
  }

  // Add new track
  private post(song: Song): Promise<Song> {
    let headers = new Headers({
      'Content-Type': 'application/json'});
    console.log(song);
    return this.http
               .post(this.songSaveUrl, JSON.stringify(song), {headers: headers})
               .toPromise()
               .then(res => res.json())
               .catch(this.handleError);
  }

  // Get Song List
  get(): Promise<Song[]> {
    let headers = new Headers({
      'Content-Type': 'application/json'});
    return this.http
               .get(this.songGetUrl,{headers: headers})
               .toPromise()
               .then(res => res.json().songs)
               .catch(this.handleError);
  }

  // Get Song Top Vote
  getTopVote(): Promise<Song> {
    let headers = new Headers({
      'Content-Type': 'application/json'});
    return this.http
               .get(this.songGetTopVoteUrl,{headers: headers})
               .toPromise()
               .then(res => res.json())
               .catch(this.handleError);
  }

  // Update song's hasPlayed status
  updateHasPlayed(song: Song) {
    let headers = new Headers({
      'Content-Type': 'application/json'});
    let furl = this.songUpdateHasPlayedUrl + song.id + '/hasPlayed/' + song.hasPlayed;
    return this.http
               .post(furl, {headers: headers})
               .toPromise()
               .then()
               .catch(this.handleError);
  }

  delete(songId: number) {
    var url = this.songDeleteUrl + songId;
    let headers = new Headers({
      'Content-Type': 'application/json'});
    this.http
            .get(url, {headers: headers})
            .toPromise()
            .then()
            .catch(this.handleError);
  }

  private handleError(error: any) {
    return Promise.reject(error.message || error);
  }
  
}