import { Component }  from '@angular/core';

@Component({
  selector: 'my-song-landing',
  templateUrl: 'song-landing.component.html'
})
export class SongLandingComponent {
  title = 'Song';
  addClicked: boolean;
  listClicked: boolean;
  
  ngOnInit() {
    this.addClicked = true;
    this.listClicked = false;
  }

  onAddClick() {
    this.addClicked = true;
    this.listClicked = false;
  }

  onListClick() {
    this.addClicked = false;
    this.listClicked = true;
  }
}
