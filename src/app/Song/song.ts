
export class Song {
  id: number;
  name: string;
  votes: number;
  url: string;
  formatedDate: string;
  releaseDate: Date;
  artworkUrl: string;
  collection: string;
  genre: string;
  language: string;
  hasPlayed: boolean;
  duration: number;
  
  isEditClicked: boolean;
  isVisibleBlock: boolean;
}