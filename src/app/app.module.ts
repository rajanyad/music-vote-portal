import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { BrowserModule }  from '@angular/platform-browser';

import { AppComponent }         from './app.component';
import { routing,
         appRoutingProviders }   from './app.routes';

import { HomeComponent } from './Home/home.component';

import { SongLandingComponent } from './Song/song-landing.component';

import { SongDetailComponent } from './Song/song-detail.component';
import { SongListComponent } from './Song/song-list.component';


import { S3OperationsService }  from './Util/s3operations.service';
import { SongService }  from './Song/song.service';

import { NguiPopupModule } from '@ngui/popup';
import { UiSwitchModule } from 'ngx-ui-switch/src'

@NgModule({
  declarations: [ 
    AppComponent,
  
    HomeComponent,
    SongLandingComponent,
   
    SongDetailComponent, 
    SongListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    routing,
    NguiPopupModule,
    UiSwitchModule
  ],
  bootstrap: [ AppComponent ],
  providers: [ 
    appRoutingProviders,
    S3OperationsService,
    SongService
   ]
})
export class AppModule {}
