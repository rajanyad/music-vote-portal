import { Component }          from '@angular/core';
import './rxjs-extensions';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  title = 'MUSIC VOTE APP PORTAL';
}
