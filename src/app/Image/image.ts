export class Song {
  id: number;
  duration: number;
  genres: string;
  industry: number;
  language: number;
  lyrics: string;
  releaseDate: Date;
  title: string;
}


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/