import { MusicVotePortalPage } from './app.po';

describe('music-vote-portal App', function() {
  let page: MusicVotePortalPage;

  beforeEach(() => {
    page = new MusicVotePortalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
